# Desafio Login

Projeto criado para resolver os desafios propostos no git: [InteliPost](https://github.com/intelipost/job-backend-developer)

# Live

A API se encontra no servidor : ``` http://34.204.88.172:8080/desafio-intelipost ```

# Arquitetura

O Projeto conta com a seguinte arquitetura

- Modulo Api: Utilizado para realizar as configurações da API e os expor os serviços REST
- Modulo Service: Utilizado para encapsular toda a possível regra de negócio do projeto e utilizar o repository se necessário
- Modulo Assembler: Utilizado para transformar os objetos 'Resource' (DTO) para Entidades. 
- Modulo Repository: Utilizado para encapsular todo o tratamento com o banco de dados.
- Modulo Canonico: Utilizado para gerar as classes através dos esquemas em .xsd
- Modulo Model: Utilizado para encapsular todos as classes que serão persistidas no banco de dados
- A API é stateless, ou seja, ela não armazena o estado das requisições, sendo assim é possível escalar a API sem problemas.
- JWT - JSON Web Token é utilizado para autenticação e não é necessário persistir essa informação no banco apenas verificar se o token é valido resolvendo um dos problemas de lentidão;
- Qualquer aplicação que consiga se comunicar utilizado os padrões REST conseguem utilizar a API, ou seja, o problema de outras aplicações necessitarem ter acesso a essa informação é resolvido.
- MongoDB utilizado como o banco de dados.
- Redis utilizado como cache para evitar diversas requisições ao banco de dados no momento de autenticar os usuários


# Processo de Build

## Pré-requisitos:
- Java 8 instalado
- A máquina deve ter acesso ao servidor ``` 34.204.88.172 ``` (Caso haja algum bloqueio por meio de firewall)

## Primeiro passo - Baixar projeto
```
git clone https://gitlab.com/erickmaia05/desafio-intelipost.git
cd desafio-intelipost\desafio-intelipost\
```

## Segundo passo - Gerar .jar
```bash
mvnw clean install package spring-boot:repackage
```

Alguns testes serão feitos e a tela final deve ser parecida com esta:

```bash
[INFO] Reactor Summary:
[INFO]
[INFO] desafio-intelipost 0.0.1-SNAPSHOT .................. SUCCESS [  4.613 s]
[INFO] desafio-intelipost-util ............................ SUCCESS [  4.852 s]
[INFO] desafio-intelipost-model ........................... SUCCESS [  4.267 s]
[INFO] desafio-intelipost-canonico ........................ SUCCESS [  4.821 s]
[INFO] desafio-intelipost-assembler ....................... SUCCESS [  1.132 s]
[INFO] desafio-intelipost-repository ...................... SUCCESS [  1.658 s]
[INFO] desafio-intelipost-service ......................... SUCCESS [  1.459 s]
[INFO] desafio-intelipost-api 0.0.1-SNAPSHOT .............. SUCCESS [ 38.984 s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 01:02 min
[INFO] Finished at: 2018-06-17T14:24:15-03:00
[INFO] ------------------------------------------------------------------------
```

## Terceiro passo - Executar .jar

Agora basta executar
 
```bash
java -jar desafio-intelipost-api\target\desafio-intelipost.jar
```

A imagem deve ser parecida com a seguinte:

```
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.0.1.RELEASE)
 [...]
 [...]
 2018-06-17 14:25:44.889  INFO 16020 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path '/desafio-intelipost'
2018-06-17 14:25:44.894  INFO 16020 --- [           main] b.c.i.init.DesafioIntelipostApplication  : Started DesafioIntelipostApplication in 7.379 seconds (JVM running for 8.109)
```

## Quarto passo - Utilizar a API

Agora basta utilizar a API.
Obs: A API irá subir na porta 8080, para mudar este comportamento deve ser alterado o arquivo 'application.yml' dentro da pasta 'desafio-intelipost-api\src\main\resources'
com a seguinte alteração, importante manter a identação correta do arquivo.
```
server:
   port: ${nova_porta}
   [...]
```

# Serviços

Url base: ``34.204.88.172:8080/desafio-intelipost ``

### POST /oauth/token

Serviço responsável por gerar uma chave de acesso (access_token) que será utilizado para identificar o usuário autenticados nos outros serviços, utilizado a tecnologia JWT para geração do token.

Parametros:
```
client_id: 0
client_secret: 12345678
grant_type: password
username: ${usuario} (Ex: erick)
password: ${senha} (Ex: 12345678)
```

Resposta:
- 200 - OK - Geração do JWT com sucesso
- 400 - Bad Request - Usuário ou senha inválidos ou algum campo não foi informado

```json
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJlcmljazIiLCJqdGkiOiIwZWIyNzU1OC05YWJjLTQwNDUtYWVlZi01YzBkMmVmNGY3NzIiLCJjbGllbnRfaWQiOiIwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl19.opblb-rgatap8zQu4h4LAw_jJevC4wrr6InLpTtEmvc",
    "token_type": "bearer",
    "scope": "read write",
    "jti": "0eb27558-9abc-4045-aeef-5c0d2ef4f772"
}
```

### POST /v1/usuarios

Serviço responsável por salvar um novo usuário na base.

Parametros (body):

```json
{
	"login":"erick",
	"senha":"12345678",
	"nome": "Erick Maia da Silva",
	"email":"erickmaia2@intelipost.com.br"
}
```

Headers:
* Obrigatório

```bash
*Content-Type: application/json
```

Resposta:
- 201 - Created - Usuário criado com sucesso
- 409 - Conflict - Já existe um usuário com o mesmo login
- 422 - Unprocessable Entity - Erros de validação

### GET /v1/usuarios/self

Serviço reponsável por buscar as informações do usuário autenticado de acordo com a data de alteração (If-Modified-Since, se informado)

Headers:

```
*Accept: application/json
*Authorization: Bearer ${access_token}
If-Modified-Since: EEE, dd MMM yyyy HH:mm:ss 'GMT' Ex:(Sat, 16 Jun 2018 15:51:57 GMT)
```

Resposta:

- 200 - OK - Informações do usuário encontradas
- 401 - Unauthorized - Autorização não informada ou inválida

```json
{
    "login": "erick2",
    "nome": "Erick Maia da Silva2",
    "email": "erickmaia2@intelipost.com.br",
    "dataCriacao": "2018-06-17T16:49:09.112Z",
    "dataUltimaAlteracao": "2018-06-17T16:49:09.112Z"
}
```