package br.com.intelipost.model.usuario;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

public class UsuarioTest {
	
	private Usuario usuario;
	private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();
	
	@Before
	public void setup() {
		usuario = new Usuario();
	}
	
	@Test
	public void nao_deve_aceitar_valor_nulo_para_login() {
		usuario.setLogin(null);
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "login");
		assertThat(validateProperty, hasSize(1));
		ConstraintViolation<Usuario> constraintViolation = validateProperty.iterator().next();
		assertThat(constraintViolation.getMessage(), is("O login deve ser informado"));
	}
	
	@Test
	public void nao_deve_aceitar_valor_em_branco_para_login() {
		usuario.setLogin("");
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "login");
		assertThat(validateProperty, hasSize(1));
		ConstraintViolation<Usuario> constraintViolation = validateProperty.iterator().next();
		assertThat(constraintViolation.getMessage(), is("O login deve ser informado"));
	}
	
	@Test
	public void deve_aceitar_login_valido() {
		String login = "Erick";
		usuario.setLogin(login);
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "login");
		assertThat(validateProperty, hasSize(0));
		assertThat(login, Matchers.is(usuario.getLogin()));
	}
	
	@Test
	public void nao_deve_aceitar_senha_nula() {
		usuario.setSenha(null);
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "senha");
		assertThat(validateProperty, hasSize(1));
		List<String> messages = validateProperty.stream().map(c -> c.getMessage()).collect(Collectors.toList());
		assertThat(messages, contains("A senha deve ser informada"));
	}
	
	@Test
	public void nao_deve_aceitar_senha_vazia() {
		usuario.setSenha("");
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "senha");
		assertThat(validateProperty, hasSize(2));
		List<String> messages = validateProperty.stream().map(c -> c.getMessage()).collect(Collectors.toList());
		assertTrue(messages.containsAll(asList("A senha deve ter pelo menos 8 caracteres", "A senha deve ser informada")));
	}
	
	@Test
	public void nao_deve_aceitar_senha_com_menos_de_8_caracteres() {
		usuario.setSenha("1234");
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "senha");
		assertThat(validateProperty, hasSize(1));
		List<String> messages = validateProperty.stream().map(c -> c.getMessage()).collect(Collectors.toList());
		assertTrue(messages.contains("A senha deve ter pelo menos 8 caracteres"));
	}
	
	@Test
	public void deve_aceitar_senha_valida() {
		String senha = "12345678";
		usuario.setSenha(senha);
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "senha");
		assertTrue(validateProperty.isEmpty());
		assertThat(senha, is(usuario.getSenha()));
	}
	
	@Test
	public void nao_deve_aceitar_email_nulo() {
		usuario.setEmail(null);
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "email");
		assertThat(validateProperty, hasSize(1));
		List<String> messages = validateProperty.stream().map(c -> c.getMessage()).collect(Collectors.toList());
		assertTrue(messages.contains("O email deve ser informado"));
	}
	
	@Test
	public void nao_deve_aceitar_email_vazio() {
		usuario.setEmail("");
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "email");
		assertThat(validateProperty, hasSize(1));
		List<String> messages = validateProperty.stream().map(c -> c.getMessage()).collect(Collectors.toList());
		assertTrue(messages.contains("O email deve ser informado"));
	}
	
	@Test
	public void nao_deve_aceitar_email_invalido() {
		usuario.setEmail("teste");
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "email");
		assertThat(validateProperty, hasSize(1));
		List<String> messages = validateProperty.stream().map(c -> c.getMessage()).collect(Collectors.toList());
		assertTrue(messages.contains("Deve informar um email válido"));
	}
	
	@Test
	public void deve_aceitar_email_valido() {
		String email = "teste@teste.com.br";
		usuario.setEmail(email);
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "email");
		assertTrue(validateProperty.isEmpty());
		assertThat(email, is(usuario.getEmail()));
	}
	
	@Test
	public void nao_deve_aceitar_nome_nulo() {
		usuario.setNome(null);
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "nome");
		assertThat(validateProperty, hasSize(1));
		List<String> messages = validateProperty.stream().map(c -> c.getMessage()).collect(Collectors.toList());
		assertTrue(messages.contains("Deve informar o nome"));
	}
	
	@Test
	public void nao_deve_aceitar_nome_vazio() {
		usuario.setNome("");
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "nome");
		assertThat(validateProperty, hasSize(1));
		List<String> messages = validateProperty.stream().map(c -> c.getMessage()).collect(Collectors.toList());
		assertTrue(messages.contains("Deve informar o nome"));
	}
	
	@Test
	public void deve_aceitar_nome_valido() {
		String nome = "Erick Maia";
		usuario.setNome(nome);
		Set<ConstraintViolation<Usuario>> validateProperty = VALIDATOR.validateProperty(usuario, "nome");
		assertTrue(validateProperty.isEmpty());
		assertThat(nome, is(usuario.getNome()));
	}
	
	@Test
	public void deve_ter_mesmo_hash_code_para_objeto_iguais() {
		usuario.setLogin("erick");
		Usuario outro = new Usuario();
		outro.setLogin("erick");
		assertThat(usuario.hashCode(), is(outro.hashCode()));
		assertTrue(usuario.equals(outro));
	}
	
	@Test
	public void deve_ter_hash_code_diferente_para_objetos_diferentes() {
		usuario.setLogin("erick");
		Usuario outro = new Usuario();
		outro.setLogin("outro");
		assertThat(usuario.hashCode(), not(is(outro.hashCode())));
		assertFalse(usuario.equals(outro));
	}
	
}
