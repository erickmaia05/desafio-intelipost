package br.com.intelipost.model.usuario;

import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.Length;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.intelipost.util.json.JsonToStringStyle;

@Document
public class Usuario implements UserDetails {

	private static final long serialVersionUID = 1L;
	
	@Id
	@NotBlank(message = "O login deve ser informado")
	private String login;
	
	@NotBlank(message = "A senha deve ser informada")
	@Length(min=8, message="A senha deve ter pelo menos {min} caracteres")
	private String senha;
	
	@NotBlank(message = "O email deve ser informado")
	@Email(message="Deve informar um email válido")
	private String email;
	
	@NotBlank(message = "Deve informar o nome")
	private String nome;
	
	private DateTime dataCriacao;
	private DateTime dataAlteracao;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public DateTime getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(DateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public DateTime getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(DateTime dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}
	
	@Override
	public String toString() {
		return reflectionToString(this, new JsonToStringStyle());
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(login).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Usuario)) {
			return false;
		}
		Usuario other = (Usuario)obj;
		return new EqualsBuilder().append(this.login, other.login).isEquals();
	}

	@Override
	public String getPassword() {
		return this.senha;
	}

	@Override
	public String getUsername() {
		return this.login;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return new ArrayList<>(); //Não implementado
	}

	@Override
	public boolean isAccountNonExpired() {
		return true; //Não implementado
	}

	@Override
	public boolean isAccountNonLocked() {
		return true; //Não implementado
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true; //Não implementado
	}

	@Override
	public boolean isEnabled() {
		return true; //Não implementado
	}

}
