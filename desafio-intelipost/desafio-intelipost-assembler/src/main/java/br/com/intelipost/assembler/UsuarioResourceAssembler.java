package br.com.intelipost.assembler;

import org.springframework.stereotype.Component;

import br.com.intelipost.canonico.usuario.v1.UsuarioResource;
import br.com.intelipost.model.usuario.Usuario;

@Component
public class UsuarioResourceAssembler {

	public Usuario toEntity(UsuarioResource resource) {
		Usuario entity = null;
		if (resource != null) {
			entity = new Usuario();
			entity.setLogin(resource.getLogin());
			entity.setEmail(resource.getEmail());
			entity.setSenha(resource.getSenha());
			entity.setNome(resource.getNome());
		}
		return entity;
	}

	public UsuarioResource toResource(Usuario entity) {
		UsuarioResource resource = null;
		if (entity != null) {
			resource = new UsuarioResource();
			resource.setDataCriacao(entity.getDataCriacao());
			resource.setDataUltimaAlteracao(entity.getDataAlteracao());
			resource.setEmail(entity.getEmail());
			resource.setLogin(entity.getLogin());
			resource.setNome(entity.getNome());
		}
		return resource;
	}
}
