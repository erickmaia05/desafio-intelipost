package br.com.intelipost.repository.usuario;

import org.joda.time.DateTime;

import br.com.intelipost.model.usuario.Usuario;

public interface UsuarioRepositoryCustom {
	
	Usuario buscarUsuario(String id, DateTime dataAlteracao);
	
}
