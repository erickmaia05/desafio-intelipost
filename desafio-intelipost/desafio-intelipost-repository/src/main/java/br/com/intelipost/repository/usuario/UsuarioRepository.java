package br.com.intelipost.repository.usuario;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.intelipost.model.usuario.Usuario;

public interface UsuarioRepository extends MongoRepository<Usuario, String>, UsuarioRepositoryCustom {

}
