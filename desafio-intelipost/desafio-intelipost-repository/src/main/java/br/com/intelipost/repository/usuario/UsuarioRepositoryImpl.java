package br.com.intelipost.repository.usuario;

import static br.com.intelipost.util.datetime.DateTimeUtil.setMaxMillis;
import static org.springframework.data.mongodb.core.query.Criteria.where;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import br.com.intelipost.model.usuario.Usuario;

public class UsuarioRepositoryImpl implements UsuarioRepositoryCustom {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Usuario buscarUsuario(String id, DateTime dataAlteracao) {
		Query query = new Query(where("_id").is(id));
		if (dataAlteracao != null) {
			query.addCriteria(where("dataAlteracao").gt(setMaxMillis(dataAlteracao)));
		}
		return mongoTemplate.findOne(query, Usuario.class);
	}

}
