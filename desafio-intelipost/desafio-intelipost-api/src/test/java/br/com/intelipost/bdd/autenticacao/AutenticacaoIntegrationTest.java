package br.com.intelipost.bdd.autenticacao;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = { "src/test/resources/features/autenticacao.feature" })
public class AutenticacaoIntegrationTest {

}
