package br.com.intelipost.bdd.steps;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.web.client.HttpClientErrorException;

public class AutenticacaoSteps {

	public static final String CLIENT_ID = "0";
	public static final String CLIENT_SECRET = "12345678";
	public static final String GRANT_TYPE = "password";
	public static final String DEFAULT_USERNAME = "erick";
	public static final String DEFAULT_PASSWORD = "12345678";
	private String usuario;
	private String senha;
	private HttpClientErrorException error;
	private DefaultOAuth2AccessToken token;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public HttpClientErrorException getError() {
		return error;
	}

	public void setError(HttpClientErrorException error) {
		this.error = error;
	}

	public DefaultOAuth2AccessToken getToken() {
		return token;
	}

	public void setToken(DefaultOAuth2AccessToken token) {
		this.token = token;
	}
	
}
