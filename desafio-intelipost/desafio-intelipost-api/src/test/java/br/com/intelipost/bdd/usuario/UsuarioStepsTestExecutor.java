package br.com.intelipost.bdd.usuario;

import static br.com.intelipost.bdd.steps.AutenticacaoSteps.CLIENT_ID;
import static br.com.intelipost.bdd.steps.AutenticacaoSteps.CLIENT_SECRET;
import static br.com.intelipost.bdd.steps.AutenticacaoSteps.DEFAULT_PASSWORD;
import static br.com.intelipost.bdd.steps.AutenticacaoSteps.DEFAULT_USERNAME;
import static br.com.intelipost.bdd.steps.AutenticacaoSteps.GRANT_TYPE;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.intelipost.bdd.steps.UsuarioSteps;
import br.com.intelipost.canonico.usuario.v1.UsuarioResource;
import br.com.intelipost.init.DesafioIntelipostApplication;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

@SpringBootTest(webEnvironment = RANDOM_PORT, classes = { DesafioIntelipostApplication.class })
public class UsuarioStepsTestExecutor {
	
	@Steps
	private UsuarioSteps steps;
	
	@Autowired
	private TestRestTemplate restTemplate;

	@Dado("^que o usuario esta valido$")
	public void usuario_valido() {
		steps.generateUsuarioValido();
	}

	@Dado("^que estou logado com um usuario valido$")
	public void estou_logado_com_usuario_valido() throws Throwable {
		UriComponentsBuilder builder = createUrl();
		HttpEntity<Void> request = createRequest();
		ResponseEntity<DefaultOAuth2AccessToken> response = restTemplate.postForEntity(builder.build().toUriString(),
				request, DefaultOAuth2AccessToken.class);
		steps.setToken(response.getBody());
	}
	
	@Dado("^que nao estou logado$")
	public void queNaoEstouLogado() throws Throwable {
		steps.setToken(null);
	}
	
	@Dado("^que o usuario nao esta valido$")
	public void queOUsuarioNaoEstaValido() throws Throwable {
		steps.generateUsuarioInvalido();
	}

	@Quando("^salvar usuario$")
	public void salvar_usuario() throws Throwable {
		MultiValueMap<String, String> headers = createDefaultHeaders();
		headers.add(CONTENT_TYPE, APPLICATION_JSON_VALUE);
		HttpEntity<UsuarioResource> request = new HttpEntity<>(steps.getUsuario(), headers);
		ResponseEntity<Void> response = restTemplate.exchange("/v1/usuarios", POST, request, Void.class);
		steps.setResponse(response);
	}

	@Quando("^buscar as informacoes do usuario logado$")
	public void buscarAsInformacoesDoUsuarioLogado() throws Throwable {
		MultiValueMap<String, String> headers = createDefaultHeaders();
		headers.add(CONTENT_TYPE, APPLICATION_JSON_VALUE);
		if (steps.getToken() != null) {
			headers.add(AUTHORIZATION, steps.getToken().getTokenType() + " " + steps.getToken().getValue());
		}
		HttpEntity<Void> request = new HttpEntity<>(headers);
		ResponseEntity<UsuarioResource> response = restTemplate.exchange("/v1/usuarios/self", GET, request, UsuarioResource.class);
		steps.setResponse(response);
	}
	
	@Entao("^deve receber salvar usuario com sucesso$")
	public void salvar_usuario_com_sucesso() throws Throwable {
		checkResult(CREATED);
	}

	@Entao("^nao deve deve salvar usuario$")
	public void naoDeveDeveSalvarUsuario() throws Throwable {
		checkResult(UNPROCESSABLE_ENTITY);
	}

	@Entao("^devo receber as informacoes do meu usuario$")
	public void devoReceberAsInformacoesDoMeuUsuario() throws Throwable {
		checkResult(OK);
		assertNotNull(steps.getResponse().getBody());
	}

	@Entao("^devo receber um erro$")
	public void devoReceberUmErro() throws Throwable {
		checkResult(UNAUTHORIZED);
	}
	
	private void checkResult(HttpStatus expected) {
		assertNotNull(steps.getResponse());
		assertTrue(expected.equals(steps.getResponse().getStatusCode()));
	}
	
	private HttpEntity<Void> createRequest() {
		MultiValueMap<String, String> headers = createDefaultHeaders();
		HttpEntity<Void> request = new HttpEntity<>(headers);
		return request;
	}

	private MultiValueMap<String, String> createDefaultHeaders() {
		MultiValueMap<String, String> headers = new HttpHeaders();
		headers.add(ACCEPT, APPLICATION_JSON_VALUE);
		return headers;
	}

	private UriComponentsBuilder createUrl() {
		UriComponentsBuilder builder = UriComponentsBuilder.fromPath("/oauth/token");
		builder.queryParam("client_id", CLIENT_ID);
		builder.queryParam("client_secret", CLIENT_SECRET);
		builder.queryParam("grant_type", GRANT_TYPE);
		builder.queryParam("username", DEFAULT_USERNAME);
		builder.queryParam("password", DEFAULT_PASSWORD);
		return builder;
	}

}
