package br.com.intelipost.bdd.steps;

import static java.util.Locale.getDefault;

import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;

import com.github.javafaker.Faker;

import br.com.intelipost.canonico.usuario.v1.UsuarioResource;

public class UsuarioSteps {
	
	private UsuarioResource usuario;
	private DefaultOAuth2AccessToken token;
	private ResponseEntity<?> response;

	public void generateUsuarioValido() {
		Faker faker = new Faker(getDefault());
		usuario = new UsuarioResource();
		usuario.setNome(faker.name().fullName());
		usuario.setLogin(faker.number().randomNumber() + faker.name().firstName() + faker.number().randomNumber(5, false));
		usuario.setSenha(faker.number().digits(9));
		usuario.setEmail(faker.name().firstName() +"@intelipost.com.br");
	}
	
	public void generateUsuarioInvalido() {
		usuario = new UsuarioResource();
		usuario.setLogin(null);
	}
	
	public UsuarioResource getUsuario() {
		return usuario;
	}
	
	public void setUsuario(UsuarioResource usuario) {
		this.usuario = usuario;
	}

	public DefaultOAuth2AccessToken getToken() {
		return token;
	}

	public void setToken(DefaultOAuth2AccessToken token) {
		this.token = token;
	}

	public ResponseEntity<?> getResponse() {
		return response;
	}

	public void setResponse(ResponseEntity<?> response) {
		this.response = response;
	}
	
}
