package br.com.intelipost.bdd.autenticacao;

import static br.com.intelipost.bdd.steps.AutenticacaoSteps.CLIENT_ID;
import static br.com.intelipost.bdd.steps.AutenticacaoSteps.CLIENT_SECRET;
import static br.com.intelipost.bdd.steps.AutenticacaoSteps.GRANT_TYPE;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.intelipost.bdd.steps.AutenticacaoSteps;
import br.com.intelipost.init.DesafioIntelipostApplication;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

@SpringBootTest(webEnvironment = RANDOM_PORT, classes = { DesafioIntelipostApplication.class })
public class AutenticacaoStepsTestExecutor {

	@Steps
	private AutenticacaoSteps steps;

	@Autowired
	private TestRestTemplate restTemplate;

	@Dado("^que o usuario '(.+)' com a senha '(.+)'$")
	public void armazenar_usuario(String usuario, String senha) throws Throwable {
		steps.setUsuario(usuario);
		steps.setSenha(senha);
	}

	@Quando("^realizar login$")
	public void realizar_login() throws Throwable {
		UriComponentsBuilder builder = createUrl();
		HttpEntity<Void> request = createRequest();
		ResponseEntity<DefaultOAuth2AccessToken> response = restTemplate.postForEntity(builder.build().toUriString(),
				request, DefaultOAuth2AccessToken.class);
		if (response.getStatusCode() != OK) {
			steps.setError(new HttpClientErrorException(BAD_REQUEST));
		} else {
			steps.setToken(response.getBody());
		}
	}

	@Entao("^deve receber seu token de acesso$")
	public void devo_receber_token() throws Throwable {
		assertTrue(steps.getToken() != null && isNotEmpty(steps.getToken().getValue()));
	}

	@Entao("^nao deve receber seu token de acesso$")
	public void naoDeveReceberSeuTokenDeAcesso() throws Throwable {
		assertTrue(steps.getToken() == null && steps.getError() != null);
	}

	private HttpEntity<Void> createRequest() {
		MultiValueMap<String, String> headers = new HttpHeaders();
		headers.add(ACCEPT, APPLICATION_JSON_VALUE);
		HttpEntity<Void> request = new HttpEntity<>(headers);
		return request;
	}

	private UriComponentsBuilder createUrl() {
		UriComponentsBuilder builder = UriComponentsBuilder.fromPath("/oauth/token");
		builder.queryParam("client_id", CLIENT_ID);
		builder.queryParam("client_secret", CLIENT_SECRET);
		builder.queryParam("grant_type", GRANT_TYPE);
		builder.queryParam("username", steps.getUsuario());
		builder.queryParam("password", steps.getSenha());
		return builder;
	}

}
