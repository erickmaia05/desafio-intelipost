#language: pt
Funcionalidade: Usuario
	Feature para realizar os teste sobre os serviços de usuário

  Cenario: Deve salvar usuario
    Dado que o usuario esta valido
    Quando salvar usuario
    Entao deve receber salvar usuario com sucesso
    
  Cenario: Nao deve salvar usuario invalido
  	Dado que o usuario nao esta valido
  	Quando salvar usuario
  	Entao nao deve deve salvar usuario
  	
  Cenario: Deve buscar informacoes do usuario logado
  	Dado que estou logado com um usuario valido
  	Quando buscar as informacoes do usuario logado
  	Entao devo receber as informacoes do meu usuario
  	
  Cenario: Nao deve buscar informacos do usuario quando nao estiver logado
  	Dado que nao estou logado
  	Quando buscar as informacoes do usuario logado
  	Entao devo receber um erro
