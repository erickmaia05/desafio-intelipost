#language: pt
Funcionalidade: Autenticacao
	Feature para realizar os teste sobre autenticação

  Cenario: Deve autenticar usuario
    Dado que o usuario 'erick' com a senha '12345678'
    Quando realizar login
    Entao deve receber seu token de acesso
    
  Cenario: Nao deve autenticar usuario invalido
  	Dado que o usuario 'invalido' com a senha 'invalido'
  	Quando realizar login
  	Entao nao deve receber seu token de acesso
  	
  

