package br.com.intelipost.init;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "br.com.intelipost.config", "br.com.intelipost.controller", 
		"br.com.intelipost.service", "br.com.intelipost.assembler"})
@EnableMongoRepositories(basePackages = { "br.com.intelipost.repository" })
@EnableCaching
public class DesafioIntelipostApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioIntelipostApplication.class, args);
	}
}
