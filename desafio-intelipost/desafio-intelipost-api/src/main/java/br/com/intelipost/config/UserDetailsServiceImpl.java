package br.com.intelipost.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import br.com.intelipost.model.usuario.Usuario;
import br.com.intelipost.service.usuario.UsuarioService;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private UsuarioService usuarioService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioService.buscarUsuario(username);
		if (usuario != null) {
			return usuario;
		}
		throw new UsernameNotFoundException("Usuario não encontrado");
	}

}
