package br.com.intelipost.controller;

import static br.com.intelipost.util.datetime.DateTimeUtil.parseIfModifiedSince;
import static br.com.intelipost.util.exception.PreconditionsRest.checkNotFoundOrNotModified;
import static org.springframework.http.HttpHeaders.IF_MODIFIED_SINCE;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.intelipost.assembler.UsuarioResourceAssembler;
import br.com.intelipost.canonico.usuario.v1.UsuarioResource;
import br.com.intelipost.model.usuario.Usuario;
import br.com.intelipost.service.usuario.UsuarioService;

@RestController
@RequestMapping("/v1/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private UsuarioResourceAssembler usuarioResourceAssembler;

	@PostMapping(consumes = APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> salvarUsuario(@RequestBody UsuarioResource usuarioResource) {
		Usuario usuario = usuarioResourceAssembler.toEntity(usuarioResource);
		usuarioService.salvarUsuario(usuario);
		return new ResponseEntity<>(CREATED);
	}

	@GetMapping(value = "/self",produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<UsuarioResource> buscarUsuarioAutenticado(@RequestHeader(value = IF_MODIFIED_SINCE, required = false) String ifModifiedSince) {
		String login = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		DateTime dataAlteracao = parseIfModifiedSince(ifModifiedSince);
		Usuario usuario = usuarioService.buscarUsuario(login, dataAlteracao);
		checkNotFoundOrNotModified(dataAlteracao, usuario, "Usuário não encontrado", "Sem novas alterações");
		return ResponseEntity.ok(usuarioResourceAssembler.toResource(usuario));
	}

}
