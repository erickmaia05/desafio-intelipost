package br.com.intelipost.config;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import br.com.intelipost.util.exception.ResourceAccessDeniedException;
import br.com.intelipost.util.exception.ResourceBusinessException;
import br.com.intelipost.util.exception.ResourceConflictException;
import br.com.intelipost.util.exception.ResourceNotFoundException;
import br.com.intelipost.util.exception.ResourceNotModifiedException;
import br.com.intelipost.util.exception.handler.ResourceAccessDeniedExceptionHandler;
import br.com.intelipost.util.exception.handler.ResourceBusinessExceptionHandler;
import br.com.intelipost.util.exception.handler.ResourceConflictExceptionHandler;
import br.com.intelipost.util.exception.handler.ResourceNotFoundExceptionHandler;
import br.com.intelipost.util.exception.handler.ResourceNotModifiedExceptionHandler;
import cz.jirutka.spring.exhandler.RestHandlerExceptionResolver;

@Configuration
public class RestContextConfig implements WebMvcConfigurer {

	@Override
	public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> resolvers) {
		resolvers.add(restExceptionResolver());
	}

	@Bean
	public RestHandlerExceptionResolver restExceptionResolver() {
		return RestHandlerExceptionResolver.builder().defaultContentType(APPLICATION_JSON)
				.addErrorMessageHandler(InvalidGrantException.class, BAD_REQUEST)
				.addHandler(ResourceNotFoundException.class, new ResourceNotFoundExceptionHandler())
				.addHandler(ResourceBusinessException.class, new ResourceBusinessExceptionHandler())
				.addHandler(ResourceConflictException.class, new ResourceConflictExceptionHandler())
				.addHandler(ResourceAccessDeniedException.class, new ResourceAccessDeniedExceptionHandler())
				.addHandler(ResourceNotModifiedException.class, new ResourceNotModifiedExceptionHandler()).withDefaultHandlers(false).build();
	}
	

}
