package br.com.intelipost.service.usuario;

import static br.com.intelipost.util.exception.PreconditionsRest.checkConflict;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
import static org.springframework.util.CollectionUtils.isEmpty;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.intelipost.model.usuario.Usuario;
import br.com.intelipost.repository.usuario.UsuarioRepository;
import br.com.intelipost.util.exception.ResourceBusinessException;
import br.com.intelipost.util.exception.message.ValidationErrorMessageCustom;
import br.com.intelipost.util.exception.message.ValidationErrorMessageCustom.Error;
import br.com.intelipost.util.validation.RestValidationUtil;

@Service
public class UsuarioService {

	private static final String CAMPOS_INVALIDOS = "Campos inválidos";

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public void salvarUsuario(Usuario usuario) {
		validateUsuario(usuario);
		checkConflict(usuarioRepository.findById(usuario.getLogin()).orElse(null), "Já existe um usuário com este login, por favor, utilize outro.");
		DateTime now = DateTime.now();
		usuario.setSenha(passwordEncoder.encode(usuario.getSenha()));
		usuario.setDataAlteracao(now);
		usuario.setDataCriacao(now);
		usuarioRepository.save(usuario);
	}
	
	public Usuario buscarUsuario(String login, DateTime dataAlteracao) {
		return usuarioRepository.buscarUsuario(login, dataAlteracao);
	}

	@Cacheable(cacheNames = "usuarioCache")
	public Usuario buscarUsuario(String login) {
		return usuarioRepository.findById(login).orElse(null);
	}

	public void validateUsuario(Usuario usuario) {
		Set<ConstraintViolation<Object>> validateEntity = RestValidationUtil.validateEntity(usuario);
		if (!isEmpty(validateEntity)) {
			ValidationErrorMessageCustom erro = new ValidationErrorMessageCustom();
			erro.setTitle(CAMPOS_INVALIDOS);
			erro.setDetail(CAMPOS_INVALIDOS);
			erro.setErrors(getListErros(validateEntity));
			erro.setStatus(UNPROCESSABLE_ENTITY);
			throw new ResourceBusinessException(erro);
		}

	}

	public static List<ValidationErrorMessageCustom.Error> getListErros(Set<ConstraintViolation<Object>> errors) {
		if (!isEmpty(errors)) {
			List<Error> collect = errors.stream().map(constraintViolation -> {
				ValidationErrorMessageCustom.Error error = new ValidationErrorMessageCustom.Error();
				error.setField(constraintViolation.getPropertyPath().toString());
				error.setMessage(constraintViolation.getMessage());
				error.setRejected(constraintViolation.getInvalidValue());
				return error;
			}).collect(toList());
			return collect;
		}
		return new ArrayList<>();
	}

}
