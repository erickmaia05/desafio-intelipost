package br.com.intelipost.util.exception.handler;

import static br.com.intelipost.util.exception.rfc.RFC2616Type.CONFLICT_SPEC;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.FORBIDDEN;

import javax.servlet.http.HttpServletRequest;

import br.com.intelipost.util.exception.ResourceAccessDeniedException;
import br.com.intelipost.util.exception.util.ResourceHandlerUtil;
import cz.jirutka.spring.exhandler.handlers.ErrorMessageRestExceptionHandler;
import cz.jirutka.spring.exhandler.messages.ErrorMessage;
import cz.jirutka.spring.exhandler.messages.ValidationErrorMessage;

public class ResourceAccessDeniedExceptionHandler
		extends ErrorMessageRestExceptionHandler<ResourceAccessDeniedException> {

	private ErrorMessage errorMessage;

	public ResourceAccessDeniedExceptionHandler() {
		super(FORBIDDEN);
	}

	@Override
	public ErrorMessage createBody(ResourceAccessDeniedException ex, HttpServletRequest req) {
		ErrorMessage tmpl = super.createBody(ex, req);
		ValidationErrorMessage msg = new ValidationErrorMessage(tmpl);
		errorMessage = new ErrorMessage();
		if (errorMessage.getDetail() == null) {
			errorMessage.setDetail(ex.getMessage());
		}
		msg.setType(ResourceHandlerUtil.getType(errorMessage, CONFLICT_SPEC));
		msg.setTitle(ResourceHandlerUtil.getTitle(errorMessage, CONFLICT));
		msg.setDetail(ResourceHandlerUtil.getDetail(errorMessage, CONFLICT));
		return msg;
	}

}
