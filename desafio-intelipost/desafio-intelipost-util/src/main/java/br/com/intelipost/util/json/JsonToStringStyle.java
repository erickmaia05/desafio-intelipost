package br.com.intelipost.util.json;

import static org.apache.commons.lang3.StringUtils.defaultString;

import java.util.Collection;

import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

public class JsonToStringStyle extends ToStringStyle {

    private static final long serialVersionUID = 1L;

    private static final String QUOTES = "\"";

    private String fieldNamePrefix;
    
    private String fieldNameSuffix;

    public JsonToStringStyle() {
        setUseClassName(false);
        setUseIdentityHashCode(false);
        setContentStart("{");
        setContentEnd("}");
        setArrayStart("[");
        setArrayEnd("]");
        setFieldSeparator(",");
        setFieldNameValueSeparator(":");
        setNullText("null");
        setFieldNamePrefix(QUOTES);
        setFieldNameSuffix(QUOTES);
    }

    public String getFieldNamePrefix() {
        return fieldNamePrefix;
    }

    public void setFieldNamePrefix(String fieldNamePrefix) {
        this.fieldNamePrefix = defaultString(fieldNamePrefix, "");
    }

    public String getFieldNameSuffix() {
        return fieldNameSuffix;
    }

    public void setFieldNameSuffix(String fieldNameSuffix) {
        this.fieldNameSuffix = defaultString(fieldNameSuffix, "");
    }

    private String decorateFieldName(String fieldName) {
        return getFieldNamePrefix() + fieldName + getFieldNameSuffix();
    }

    @Override
    protected void appendFieldStart(StringBuffer buffer, String fieldName) {
        super.appendFieldStart(buffer, decorateFieldName(fieldName));
    }

    @Override
    protected void appendDetail(StringBuffer buffer, String fieldName, Object value) {

        if (value == null) {
            appendNullText(buffer, fieldName);
            return;
        }
        if (value instanceof String || value instanceof DateTime || value instanceof LocalDateTime || value instanceof Enum) {
            appendValueAsString(buffer, value);
            return;
        }
        buffer.append(value);
    }

    @Override
    protected void appendDetail(StringBuffer buffer, String fieldName, Collection<?> coll) {
        appendDetail(buffer, fieldName, coll.toArray());
    }

    /**
     * Append value as string.
     * 
     * @param buffer O(a)(s) buffer
     * @param value O(a)(s) value
     */
    private void appendValueAsString(StringBuffer buffer, Object value) {
        buffer.append(QUOTES + value.toString() + QUOTES);
    }

}
