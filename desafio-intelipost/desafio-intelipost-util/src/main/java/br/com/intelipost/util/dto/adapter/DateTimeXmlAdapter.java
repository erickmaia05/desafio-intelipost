package br.com.intelipost.util.dto.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;

public class DateTimeXmlAdapter extends XmlAdapter<String, DateTime> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.
	 * Object)
	 */
	@Override
	public DateTime unmarshal(String value) throws Exception {
		return DateTime.parse(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.
	 * Object)
	 */
	@Override
	public String marshal(DateTime value) throws Exception {
		return value.toString();
	}

}
