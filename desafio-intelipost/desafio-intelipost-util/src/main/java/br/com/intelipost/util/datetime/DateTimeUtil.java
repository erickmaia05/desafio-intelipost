package br.com.intelipost.util.datetime;

import static java.util.Locale.US;
import static org.joda.time.DateTimeZone.forID;
import static org.joda.time.format.DateTimeFormat.forPattern;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

public final class DateTimeUtil {

	public static final String HTTP_HEADER_DATE = "EEE, dd MMM yyyy HH:mm:ss 'GMT'";

	private DateTimeUtil() { }

	public static DateTime parseIfModifiedSince(String date) {
		if (date == null) {
			return null;
		}
		DateTimeFormatter rfc822DateFormatter = forPattern(HTTP_HEADER_DATE).withLocale(US).withZone(forID("GMT"));
		return rfc822DateFormatter.parseDateTime(date);
	}
	
	public static DateTime setMaxMillis(DateTime dateTime) {
		return dateTime != null ? dateTime.millisOfSecond().withMaximumValue() : dateTime;
	}

}