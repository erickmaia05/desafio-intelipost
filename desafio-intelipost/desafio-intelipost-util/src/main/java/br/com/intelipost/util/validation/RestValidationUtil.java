package br.com.intelipost.util.validation;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

public final class RestValidationUtil {
	
	private RestValidationUtil() { }
	
	public static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    public static Set<ConstraintViolation<Object>> validate(Object resource, Class<?>... clazz) {
        return VALIDATOR.validate(resource, clazz);
    }
    
    public static Set<ConstraintViolation<Object>> validateEntity(Object resource) {
        return VALIDATOR.validate(resource);
    }

}
