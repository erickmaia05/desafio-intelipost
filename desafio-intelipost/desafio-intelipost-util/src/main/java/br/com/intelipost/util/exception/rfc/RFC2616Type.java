package br.com.intelipost.util.exception.rfc;

public final class RFC2616Type {

    private RFC2616Type() {
    }
    
    public static final String UNAUTHORIZED_ENTITY_SPEC = "http://tools.ietf.org/html/rfc2616#section-10.4.2";

    public static final String CONFLICT_SPEC = "http://tools.ietf.org/html/rfc2616#section-10.4.10";

    public static final String NOT_FOUND_SPEC = "http://tools.ietf.org/html/rfc2616#section-10.4.5";

    public static final String NOT_MODIFIED_SPEC = "http://tools.ietf.org/html/rfc2616#section-10.3.5";
    
    public static final String BAD_RESQUEST_SPEC = "http://tools.ietf.org/html/rfc2616#section-10.4.1";

    public static final String UNSUPPORTED_MEDIA_TYPE_SPEC = "http://tools.ietf.org/html/rfc2616#section-10.4.16";
    
    public static final String FORBIDDEN_SPEC = "http://tools.ietf.org/html/rfc2616#section-10.4.4";
    
}