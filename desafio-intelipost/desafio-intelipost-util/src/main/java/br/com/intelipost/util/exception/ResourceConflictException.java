package br.com.intelipost.util.exception;

import cz.jirutka.spring.exhandler.messages.ValidationErrorMessage;

public class ResourceConflictException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private ValidationErrorMessage validationErrorMessage;

	public ResourceConflictException() {
		super();
	}

	public ResourceConflictException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ResourceConflictException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResourceConflictException(String message) {
		super(message);
	}

	public ResourceConflictException(Throwable cause) {
		super(cause);
	}

	public ResourceConflictException(ValidationErrorMessage validationErrorMessage) {
		super(validationErrorMessage.getDetail());
		this.validationErrorMessage = validationErrorMessage;
	}

	public ValidationErrorMessage getErrorMessage() {
		return this.validationErrorMessage;
	}
}
