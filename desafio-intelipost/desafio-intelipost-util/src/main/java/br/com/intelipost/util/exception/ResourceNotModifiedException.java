package br.com.intelipost.util.exception;


import cz.jirutka.spring.exhandler.messages.ValidationErrorMessage;

public class ResourceNotModifiedException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    private ValidationErrorMessage validationErrorMessage;
    
    public ResourceNotModifiedException(){
        super();
    }
    
    public ResourceNotModifiedException(String message, Throwable cause, boolean enableSuppression,boolean writableStackTrace){
        super(message,cause,enableSuppression,writableStackTrace);
    }

    public ResourceNotModifiedException(String message, Throwable cause){
        super(message,cause);
    }
    
    public ResourceNotModifiedException(String message){
        super(message);
    }
    
    public ResourceNotModifiedException(Throwable cause){
        super(cause);
    }
  
    public ResourceNotModifiedException(ValidationErrorMessage validationErrorMessage){
        super(validationErrorMessage.getDetail());
        this.validationErrorMessage = validationErrorMessage;
    }
    
    public ValidationErrorMessage getErrorMessage(){
        return this.validationErrorMessage;
    }
    
}
