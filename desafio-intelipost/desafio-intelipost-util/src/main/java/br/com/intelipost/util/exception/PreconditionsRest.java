package br.com.intelipost.util.exception;

import java.util.Collection;

import org.joda.time.DateTime;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.CollectionUtils;

import br.com.intelipost.util.exception.message.ValidationErrorMessageCustom;
import cz.jirutka.spring.exhandler.messages.ErrorMessage;

public class PreconditionsRest {

    public static <T> T checkNotFound(T reference) {
        if (reference == null) {
            throw new ResourceNotFoundException();
        }
        return reference;
    }
    
    public static <T extends Collection<?>> T checkNotFoundCollection(T collection) {
        if (CollectionUtils.isEmpty(collection)) {
            throw new ResourceNotFoundException();
        }
        return collection;
    }
    
    public static <T> T checkNotFound(T reference, ErrorMessage message) {
        if (reference == null) {
            throw new ResourceNotFoundException(message);
        }
        return reference;
    }
    
    public static <T> T checkNotFound(T reference, String message) {
        if (reference == null) {
            throw new ResourceNotFoundException(message);
        }
        return reference;
    }

    public static void checkCondition(boolean condition, String message){
        if (!condition) {
            throw new ResourceBusinessException(message);
        }
    }
    
    public static <T> T checkModify(T reference, Object errorMessage){
        if (reference == null) {
            throw new ResourceNotModifiedException(String.valueOf(errorMessage));
        }
        return reference;
    }
    
    public static void checkNotFoundOrNotModified(DateTime date, Object reference, String messageNotFound, String messageNotModified){
        if(date == null){
            checkNotFound(reference, messageNotFound);
        } else {
            checkModify(reference, messageNotModified);
        }
    }
    
    public static void checkConflict(Object reference, String message) {
    	if (reference != null) {
    		throw new ResourceConflictException(message);
    	}
    }

	public static void checkAccessDenied(boolean condition, String message) {
        if (!condition) {
            throw new AccessDeniedException(message);
        }
	}
	
	public static void checkCondition(boolean condition, ValidationErrorMessageCustom message){
        if (!condition) {
            throw new ResourceBusinessException(message);
        }
    }
    
	
}
