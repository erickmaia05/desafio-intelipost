package br.com.intelipost.util.exception;

import br.com.intelipost.util.exception.message.ValidationErrorMessageCustom;

public class ResourceBusinessException extends RuntimeException {
	
	private ValidationErrorMessageCustom validationErrorMessageCustom;

	/** A Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Instancia um novo(a) business exception.
	 */
	public ResourceBusinessException() {
		super();
	}
	
	/**
	 * Instancia um novo(a) business exception.
	 *
	 * @param message O(a)(s) message
	 * @param cause O(a)(s) cause
	 * @param enableSuppression O(a)(s) enable suppression
	 * @param writableStackTrace O(a)(s) writable stack trace
	 */
	public ResourceBusinessException(String message, Throwable cause,boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Instancia um novo(a) business exception.
	 *
	 * @param message O(a)(s) message
	 * @param cause O(a)(s) cause
	 */
	public ResourceBusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instancia um novo(a) business exception.
	 *
	 * @param message O(a)(s) message
	 */
	public ResourceBusinessException(String message) {
		super(message);
	}

	/**
	 * Instancia um novo(a) business exception.
	 *
	 * @param cause O(a)(s) cause
	 */
	public ResourceBusinessException(Throwable cause) {
		super(cause);
	}

	public ResourceBusinessException(ValidationErrorMessageCustom validationErrorMessageCustom){
	    super(validationErrorMessageCustom.getDetail());
	    this.validationErrorMessageCustom = validationErrorMessageCustom;
	}
	
	public ValidationErrorMessageCustom getErroMessageCustom(){
	    return this.validationErrorMessageCustom;
	}
	
	public void setErrorMessageCustom(ValidationErrorMessageCustom validationErrorMessageCustom){
	    this.validationErrorMessageCustom = validationErrorMessageCustom;
	}

}
