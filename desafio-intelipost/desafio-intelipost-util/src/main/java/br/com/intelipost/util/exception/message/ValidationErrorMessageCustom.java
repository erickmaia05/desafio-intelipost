package br.com.intelipost.util.exception.message;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import cz.jirutka.spring.exhandler.messages.ErrorMessage;

@JsonInclude(NON_EMPTY) //for Jackson 2.x
@XmlRootElement(name="problem") //for JAXB
public class ValidationErrorMessageCustom extends ErrorMessage {

    private static final long serialVersionUID = 1L;

    private List<Error> errors = new ArrayList<>(6);

    public ValidationErrorMessageCustom(){
        super();
    }
    
    public ValidationErrorMessageCustom(ErrorMessage orig) {
        super(orig);
    }

	public ValidationErrorMessageCustom addError(String field, Object rejectedValue, String message) {
        errors.add(new Error(field, rejectedValue, message));
        return this;
    }

    public ValidationErrorMessageCustom addError(String message) {
        errors.add(new Error(null, null, message));
        return this;
    }
    
    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @JsonInclude(NON_EMPTY)
    public static class Error {
        private String field;
        private Object rejected;
        private String message;
        
        public Error(){}
        
        public Error(String field, Object rejected, String message){
            this.field = field;
            this.rejected = rejected;
            this.message = message;
        }

        public String getField() {
            return field;
        }

        public Object getRejected() {
            return rejected;
        }

        public String getMessage() {
            return message;
        }

		public void setField(String field) {
			this.field = field;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public void setRejected(Object rejected) {
			this.rejected = rejected;
		}
        
    }
}