package br.com.intelipost.util.exception.util;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import cz.jirutka.spring.exhandler.messages.ErrorMessage;

public final class ResourceHandlerUtil {
	
	private static final Logger log = LoggerFactory.getLogger(ResourceHandlerUtil.class);
    
	private ResourceHandlerUtil() {}
	
	public static String getDetail(ErrorMessage errorMessage, HttpStatus status){
        if(errorMessage.getDetail()==null){
        	return status.getReasonPhrase();
        }else{
        	return errorMessage.getDetail();
        }
	}
	
	public static String getTitle(ErrorMessage errorMessage, HttpStatus status){
        if(errorMessage.getTitle()==null){
        	return status.getReasonPhrase();
        }else{
        	return errorMessage.getTitle();
        }
	}
	
	public static URI getType(ErrorMessage errorMessage, String spec){
		 if(errorMessage.getType()==null){
			try {
				return new URI(spec);
			} catch (URISyntaxException e) {
				log.error("Erro ao criar URI " + spec, e);
			}
		 }else{
			 return errorMessage.getType();
		 }
		return null;
	}

}
