package br.com.intelipost.util.exception.handler;

import static br.com.intelipost.util.exception.util.ResourceHandlerUtil.getDetail;
import static br.com.intelipost.util.exception.util.ResourceHandlerUtil.getTitle;
import static br.com.intelipost.util.exception.util.ResourceHandlerUtil.getType;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

import javax.servlet.http.HttpServletRequest;

import br.com.intelipost.util.exception.ResourceBusinessException;
import br.com.intelipost.util.exception.message.ValidationErrorMessageCustom;
import cz.jirutka.spring.exhandler.handlers.ErrorMessageRestExceptionHandler;
import cz.jirutka.spring.exhandler.messages.ErrorMessage;

public class ResourceBusinessExceptionHandler extends ErrorMessageRestExceptionHandler<ResourceBusinessException>{
    
	public static final String UNPROCESSABLE_ENTITY_SPEC = "https://tools.ietf.org/html/rfc4918#page-78";
	
	public ResourceBusinessExceptionHandler() {
		super(UNPROCESSABLE_ENTITY);
	}

	@Override
    public ErrorMessage createBody(ResourceBusinessException ex, HttpServletRequest req) {
		ErrorMessage tmpl = super.createBody(ex, req);
		ValidationErrorMessageCustom msg = new ValidationErrorMessageCustom(tmpl);
        if(ex.getErroMessageCustom()==null) {
        	msg.setDetail(ex.getMessage());
        }
       	createBody422(msg, ex.getErroMessageCustom());
        return msg;
    }
	
	public static void createBody422(ValidationErrorMessageCustom msg, ValidationErrorMessageCustom msgCustom) {
		ValidationErrorMessageCustom value = null;
		if (msgCustom == null) {
			value = msg;
		} else {
			value = msgCustom;
		}
		msg.setType(getType(value,UNPROCESSABLE_ENTITY_SPEC));
       	msg.setTitle(getTitle(value,UNPROCESSABLE_ENTITY));
       	msg.setDetail(getDetail(value,UNPROCESSABLE_ENTITY));
       	msg.setStatus(UNPROCESSABLE_ENTITY);
       	msg.setErrors(value.getErrors());
	}


}
