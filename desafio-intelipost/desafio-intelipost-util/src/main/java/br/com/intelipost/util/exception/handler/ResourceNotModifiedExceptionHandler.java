package br.com.intelipost.util.exception.handler;

import static br.com.intelipost.util.exception.rfc.RFC2616Type.NOT_MODIFIED_SPEC;
import static br.com.intelipost.util.exception.util.ResourceHandlerUtil.getDetail;
import static br.com.intelipost.util.exception.util.ResourceHandlerUtil.getTitle;
import static br.com.intelipost.util.exception.util.ResourceHandlerUtil.getType;
import static org.springframework.http.HttpStatus.NOT_MODIFIED;

import javax.servlet.http.HttpServletRequest;

import br.com.intelipost.util.exception.ResourceNotModifiedException;
import cz.jirutka.spring.exhandler.handlers.ErrorMessageRestExceptionHandler;
import cz.jirutka.spring.exhandler.messages.ErrorMessage;
import cz.jirutka.spring.exhandler.messages.ValidationErrorMessage;

public class ResourceNotModifiedExceptionHandler
		extends ErrorMessageRestExceptionHandler<ResourceNotModifiedException> {

	public ResourceNotModifiedExceptionHandler() {
		super(NOT_MODIFIED);
	}

	@Override
	public ErrorMessage createBody(ResourceNotModifiedException ex, HttpServletRequest req) {
		ErrorMessage tmpl = super.createBody(ex, req);
		ValidationErrorMessage msg = new ValidationErrorMessage(tmpl);
		ErrorMessage errorMessage = ex.getErrorMessage();
		if (errorMessage == null) {
			errorMessage = new ErrorMessage();
		}
		msg.setType(getType(errorMessage, NOT_MODIFIED_SPEC));
		msg.setTitle(getTitle(errorMessage, NOT_MODIFIED));
		msg.setDetail(getDetail(errorMessage, NOT_MODIFIED));
		if (ex.getErrorMessage() != null) {
			msg.setErrors(ex.getErrorMessage().getErrors());
		}
		return msg;
	}

}
