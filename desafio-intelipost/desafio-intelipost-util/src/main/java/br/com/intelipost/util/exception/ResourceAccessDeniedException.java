package br.com.intelipost.util.exception;


import cz.jirutka.spring.exhandler.messages.ValidationErrorMessage;

public class ResourceAccessDeniedException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	private ValidationErrorMessage validationErrorMessage;
	
	public ResourceAccessDeniedException() {
		super();
	}
	public ResourceAccessDeniedException(String message, Throwable cause,boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ResourceAccessDeniedException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResourceAccessDeniedException(String message) {
		super(message);
	}

	public ResourceAccessDeniedException(Throwable cause) {
		super(cause);
	}
	
	public ResourceAccessDeniedException(ValidationErrorMessage validationErrorMessage){
		super(validationErrorMessage.getDetail());
		this.validationErrorMessage = validationErrorMessage;
	}
	
	public ValidationErrorMessage getErrorMessage() {
		return this.validationErrorMessage;
	}
}
