package br.com.intelipost.util.exception.handler;

import static br.com.intelipost.util.exception.rfc.RFC2616Type.CONFLICT_SPEC;
import static org.springframework.http.HttpStatus.CONFLICT;

import javax.servlet.http.HttpServletRequest;

import br.com.intelipost.util.exception.ResourceConflictException;
import br.com.intelipost.util.exception.util.ResourceHandlerUtil;
import cz.jirutka.spring.exhandler.handlers.ErrorMessageRestExceptionHandler;
import cz.jirutka.spring.exhandler.messages.ErrorMessage;
import cz.jirutka.spring.exhandler.messages.ValidationErrorMessage;

public class ResourceConflictExceptionHandler extends ErrorMessageRestExceptionHandler<ResourceConflictException> {

	private ErrorMessage errorMessage;
	
	public ResourceConflictExceptionHandler() {
		super(CONFLICT);
	}

	@Override
	public ErrorMessage createBody(ResourceConflictException ex, HttpServletRequest req) {
		ErrorMessage tmpl = super.createBody(ex, req);
		ValidationErrorMessage msg = new ValidationErrorMessage(tmpl);
		errorMessage = ex.getErrorMessage();
		if (errorMessage == null) {
			errorMessage = new ErrorMessage();
		}
		if (errorMessage.getDetail() == null) {
			errorMessage.setDetail(ex.getMessage());
		}
		msg.setType(ResourceHandlerUtil.getType(errorMessage, CONFLICT_SPEC));
		msg.setTitle(ResourceHandlerUtil.getTitle(errorMessage, CONFLICT));
		msg.setDetail(ResourceHandlerUtil.getDetail(errorMessage, CONFLICT));
		if (ex.getErrorMessage() != null) {
			msg.setErrors(ex.getErrorMessage().getErrors());
		}
		return msg;
	}

}
