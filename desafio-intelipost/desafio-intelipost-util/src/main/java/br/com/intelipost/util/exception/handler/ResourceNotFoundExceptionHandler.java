package br.com.intelipost.util.exception.handler;

import static br.com.intelipost.util.exception.rfc.RFC2616Type.NOT_FOUND_SPEC;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import javax.servlet.http.HttpServletRequest;

import br.com.intelipost.util.exception.ResourceNotFoundException;
import br.com.intelipost.util.exception.util.ResourceHandlerUtil;
import cz.jirutka.spring.exhandler.handlers.ErrorMessageRestExceptionHandler;
import cz.jirutka.spring.exhandler.messages.ErrorMessage;
import cz.jirutka.spring.exhandler.messages.ValidationErrorMessage;

public class ResourceNotFoundExceptionHandler extends ErrorMessageRestExceptionHandler<ResourceNotFoundException> {

    private ErrorMessage errorMessage;
    
	public ResourceNotFoundExceptionHandler() {
		super(NOT_FOUND);
	}

	@Override
    public ErrorMessage createBody(ResourceNotFoundException ex, HttpServletRequest req) {
        ErrorMessage tmpl = super.createBody(ex, req);
        ValidationErrorMessage msg = new ValidationErrorMessage(tmpl);
        errorMessage=ex.getErrorMessage();
        if(errorMessage==null){
        	errorMessage = new ErrorMessage();
        }
        if(isBlank(errorMessage.getDetail())){
            errorMessage.setDetail(ex.getMessage());
        }
       	msg.setType(ResourceHandlerUtil.getType(errorMessage, NOT_FOUND_SPEC));
       	msg.setTitle(ResourceHandlerUtil.getTitle(errorMessage,NOT_FOUND));
       	msg.setDetail(errorMessage.getDetail());
        return msg;
    }
}
